FROM payara/server-full:5.192
EXPOSE 8080
EXPOSE 4848
ENV DEPLOY_DIR=/opt/payara/deployments


RUN wget https://repo1.maven.org/maven2/org/primefaces/primefaces/7.0/primefaces-7.0.jar && \
    wget https://repo1.maven.org/maven2/net/sf/jasperreports/jasperreports/6.8.0/jasperreports-6.8.0.jar && \
    wget https://repo1.maven.org/maven2/javax/javaee-api/8.0/javaee-api-8.0.jar && \
    wget https://repo1.maven.org/maven2/mysql/mysql-connector-java/5.1.48/mysql-connector-java-5.1.48.jar
    
RUN mv primefaces-7.0.jar $PAYARA_DIR/glassfish/lib && \
    mv jasperreports-6.8.0.jar $PAYARA_DIR/glassfish/lib && \
    mv javaee-api-8.0.jar $PAYARA_DIR/glassfish/lib && \
    mv mysql-connector-java-5.1.48.jar $PAYARA_DIR/glassfish/lib

RUN echo 'AS_ADMIN_PASSWORD=admin' > /opt/payara/pass.txt

WORKDIR /opt/payara/appserver/bin
RUN ./asadmin start-domain && \ 
	./asadmin -I false --user admin --passwordfile /opt/payara/pass.txt create-jdbc-connection-pool \
	--datasourceclassname=com.mysql.jdbc.jdbc2.optional.MysqlDataSource --restype=javax.sql.DataSource \
	--property='user=root:password=contraseniasecreta:url="jdbc:mysql://mariadb:3306/cine":portNumber=3306:dataBaseName=cine:driverClass=com.mysql.jdbc.Driver:serverName=mariadb' cine_pool_prn335 && \
	./asadmin -I false --user admin --passwordfile /opt/payara/pass.txt create-jdbc-resource --connectionpoolid cine_pool_prn335 jdbc/cine && \
	./asadmin -I false --user admin --passwordfile /opt/payara/pass.txt stop-domain

WORKDIR /opt/payara
COPY CineWebApp-1.0-SNAPSHOT.war $DEPLOY_DIR
