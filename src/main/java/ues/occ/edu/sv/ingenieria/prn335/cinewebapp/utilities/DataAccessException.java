/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ues.occ.edu.sv.ingenieria.prn335.cinewebapp.utilities;

import java.io.Serializable;

/**
 *
 * @author cmargueiz
 */
public class DataAccessException extends Exception implements Serializable {

    public DataAccessException(final String razon, final Throwable causa) {
        super(razon, causa);
    }

}
