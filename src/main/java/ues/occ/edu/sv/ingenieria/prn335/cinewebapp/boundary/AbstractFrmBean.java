/*
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ues.occ.edu.sv.ingenieria.prn335.cinewebapp.boundary;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import org.primefaces.event.SelectEvent;
import org.primefaces.event.UnselectEvent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;

import ues.occ.edu.sv.ingenieria.prn335.cinewebapp.utilities.EstadoCRUD;

/**
 *
 * @author cmargueiz
 */
abstract class AbstractFrmBean<T> {

    protected final static String URL_RESOURCE = "http://localhost:8080/CineWebApp/resources";
    Client client;

    public abstract Object getEntity(T object);

    public abstract T getEntityById(String rowKey);

    public abstract String getUrlResurce();

    public abstract Integer getIdEntity();

    // protected abstract AbstractFacade<T> getFacade();
    List<T> List = new ArrayList<>();
    protected T registro;
    protected LazyDataModel<T> modelo;
    protected EstadoCRUD estado;

    public void inicializar() {
        List<T> salida = null;
           try {
            System.out.println(URL_RESOURCE + getUrlResurce());

             estado = EstadoCRUD.NONE;
          
            registro = null;
        } catch (Exception e) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, e.getMessage(), e);
        }
    }

    public void onRowSelect(SelectEvent event) {

        this.registro = (T) event.getObject();
        System.out.println(registro.toString());
        this.estado = EstadoCRUD.NONE;
        

    }

    public void onRowDeselect(UnselectEvent event) {
        this.estado = EstadoCRUD.NUEVO;
        
    }

    public void btnCancelarHandler(ActionEvent ae) {
        inicializar();

        this.estado = EstadoCRUD.NONE;

    }

    public void btnCambioHandler(ActionEvent ae) {
        inicializar();
        registro = null;
        this.estado = EstadoCRUD.NUEVO;

    }

    public void btnAgregarHandler(ActionEvent ae) throws Exception {
        if (registro != null) {

            if (client != null) {
                try {
                    WebTarget target = client.target(URL_RESOURCE + getUrlResurce());
                    target.request(MediaType.APPLICATION_JSON).post(Entity.entity(registro, MediaType.APPLICATION_JSON));
                    info();
                } catch (Exception e) {
                    Logger.getLogger(getClass().getName()).log(Level.SEVERE, e.getMessage(), e);
                }
                info();
                inicializar();
            }

        }
    }

    public void btnEditarHandler(ActionEvent ae) throws Exception {
        if (registro != null) {
            try {
                WebTarget target = client.target(URL_RESOURCE + getUrlResurce()).path("update/{id}").resolveTemplate("id", getIdEntity());
                target.request(MediaType.APPLICATION_JSON).post(Entity.entity(registro, MediaType.APPLICATION_JSON));
                info();
            } catch (Exception e) {
                Logger.getLogger(getClass().getName()).log(Level.SEVERE, e.getMessage(), e);
            }
            info();
            inicializar();

        }
    }

    public void btnEliminarHandler(ActionEvent ae) {
        if (registro != null) {
            try {
                try {
                    WebTarget target = client.target(URL_RESOURCE + getUrlResurce()).path("remove/{id}").resolveTemplate("id", getIdEntity());
                    target.request().delete();

                    info();
                } catch (Exception e) {
                    Logger.getLogger(getClass().getName()).log(Level.SEVERE, e.getMessage(), e);
                }
            } catch (Exception e) {
                Logger.getLogger(getClass().getName()).log(Level.SEVERE, e.getMessage(), e);
            }
        }
    }

    public void info() {
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Info", "Proceso realizado correctamente"));
    }

//    public LazyDataModel<T> Modelo() {
//        try {
//            modelo = new LazyDataModel<T>() {
//
//                @Override
//                public Object getRowKey(T object) {
//                    if (object != null) {
//                        System.out.println("rowkey ");
//                        return getEntity(object);
//                    }
//                    return null;
//                }
//
//                @Override
//                public T getRowData(String rowKey) {
//
//                    return getEntityById(rowKey);
//                }
//
//                @Override
//                public List<T> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters) {
//                    List<T> salida = new ArrayList<>();
//
//                    try {
//                        WebTarget target = client.target(URL_RESOURCE + getUrlResurce()).path("count");
//
//                        if (getUrlResurce() != null) {
//                            this.setRowCount(target.request(MediaType.TEXT_PLAIN).get(Integer.class));
//
//                            if (this.getRowCount() > 0 && first >= this.getRowCount()) {
//                                int numberOfPages = (int) Math.ceil(this.getRowCount() * 1d / pageSize);
//                                System.out.println("Numero de paginas: " + numberOfPages);
//                                first = Math.max((numberOfPages - 1) * pageSize, 0);
//                                System.out.println("First despues " + first);
//                            }
//
//                            target = client.target(URL_RESOURCE + getUrlResurce()).path("findRange").queryParam("from", first).queryParam("to", pageSize);
//
//                            salida = target.request(MediaType.APPLICATION_JSON).get(new GenericType<List<T>>() {
//                            });
//                        }
//
//                    } catch (Exception e) {
//                        Logger.getLogger(getClass().getName()).log(Level.SEVERE, e.getMessage(), e);
//                    }
//                    return salida;
//                }
//            };
//        } catch (Exception e) {
//            Logger.getLogger(getClass().getName()).log(Level.SEVERE, e.getMessage(), e);
//        }
//        return null;
//    }

    public T getRegistro() {
        return registro;
    }

    public void setRegistro(T registro) {
        this.registro = registro;
    }

    public LazyDataModel<T> getModelo() {
        return modelo;
    }

    public void setModelo(LazyDataModel<T> modelo) {
        this.modelo = modelo;
    }

    public List<T> getList() {
        return List;
    }

    public EstadoCRUD getEstado() {
        return estado;
    }

    public void setEstado(EstadoCRUD estado) {
        this.estado = estado;
    }

}
