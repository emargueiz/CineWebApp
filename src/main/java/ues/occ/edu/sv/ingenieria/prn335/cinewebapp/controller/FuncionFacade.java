/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ues.occ.edu.sv.ingenieria.prn335.cinewebapp.controller;

import java.util.Collections;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.ws.rs.PathParam;
import ues.occ.edu.sv.ingenieria.prn335.cinewebapp.entity.AsientoSala;
import ues.occ.edu.sv.ingenieria.prn335.cinewebapp.entity.Funcion;
import ues.occ.edu.sv.ingenieria.prn335.cinewebapp.entity.Sala;

/**
 *
 * @author cmargueiz
 */
@Stateless
public class FuncionFacade extends AbstractFacade<Funcion> {

    @PersistenceContext(unitName = "CinePU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public FuncionFacade() {
        super(Funcion.class);
    }

    public List<AsientoSala> findAsientoBySala(Integer id) {
        if (id != null) {
            try {
                TypedQuery<AsientoSala> query = em.createNamedQuery("AsientoSala.findBySala", AsientoSala.class);
                query.setParameter("idSala", id);
                return query.getResultList();
            } catch (Exception e) {
                Logger.getLogger(getClass().getName()).log(Level.SEVERE, e.getMessage(), e);
            }
        }
        return Collections.EMPTY_LIST;

    }

    public List<Sala> findSalaBySucursal(Integer id) {
        if (id != null) {
            try {
                TypedQuery<Sala> query = em.createNamedQuery("Sala.findBySucursal", Sala.class);
                query.setParameter("idSucursal", id);
                return query.getResultList();
            } catch (Exception e) {
                Logger.getLogger(getClass().getName()).log(Level.SEVERE, e.getMessage(), e);
            }
        }
        return Collections.EMPTY_LIST;

    }
}
