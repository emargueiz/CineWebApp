/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ues.occ.edu.sv.ingenieria.prn335.cinewebapp.boundary;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Locale;
import java.util.Map;
import javax.faces.context.FacesContext;
import javax.faces.event.ValueChangeEvent;
import javax.faces.view.ViewScoped;
import javax.inject.Named;

/**
 *
 * @author cmargueiz
 */
@ViewScoped
@Named(value = "idiomaBean")
public class IdiomaBean  implements Serializable{
    private static final long serialVersionUID = 1L;
    private String locale;
    
    private static Map<String,Object> countries;
    static{
        countries=new LinkedHashMap<>();
        countries.put("Espanol", Locale.forLanguageTag("es"));
        countries.put("English", Locale.US);
        
}

    public String getLocale() {
        return locale;
    }

    public void setLocale(String locale) {
        this.locale = locale;
    }

    public Map<String, Object> getCountries() {
        return countries;
    }

    public void setCountries(Map<String, Object> countries) {
        IdiomaBean.countries = countries;
    }
    
    //value change event listener
   public void localeChanged(ValueChangeEvent e) {
      String newLocaleValue = e.getNewValue().toString();
      
      for (Map.Entry<String, Object> entry : countries.entrySet()) {
         
         if(entry.getValue().toString().equals(newLocaleValue)) {
            FacesContext.getCurrentInstance()
               .getViewRoot().setLocale((Locale)entry.getValue());         
         }
      }
   }
}
    
            
    

